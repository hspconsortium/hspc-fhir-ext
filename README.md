FHIR Integration
======================

Extensions to the HAPI FHIR library:

* Allows registration of custom http clients for special handling of REST calls.
* FHIR-based domain factory for materializing domain objects.
* Utility methods for accessing and manipulating FHIR objects.
* Property-based configuration support for FHIR context and clients.
* Support for accessing multiple FHIR endpoints.